import React from "react";
import { NextPage } from "next";

const AboutPage: NextPage = () => {
  return (
    <div className="py-12">
      <div className="flex flex-col items-start justify-start gap-y-2">
        <h1 className="text-2xl font-semibold">About ShadCN</h1>
        <p className="font-mono">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, hic placeat? Libero eos aliquid
          tenetur eius iure reprehenderit facilis provident velit molestias fuga atque eum voluptas, asperiores minima
          consectetur ab vel possimus ullam accusamus vero molestiae, delectus voluptates harum. Autem sapiente dolore
          dolores dolor repudiandae.
        </p>
        <p className="font-mono">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem exercitationem, velit accusantium quos
          animi itaque pariatur non ratione dolor fugit distinctio maxime nisi porro vitae sit corporis reiciendis
          veritatis? Blanditiis, sapiente. Iste voluptate debitis, praesentium accusamus ducimus porro aperiam molestias
          asperiores similique corrupti distinctio eveniet minus molestiae. Repellendus, vero quis.
        </p>
      </div>
    </div>
  );
};

export default AboutPage;
