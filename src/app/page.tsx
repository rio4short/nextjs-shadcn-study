"use client";
import { Button } from "@/components/ui/button";
import { NextPage } from "next";
import Link from "next/link";
import React from "react";

const HomePage: NextPage = () => {
  return (
    <div className="py-12">
      <div className="flex flex-col items-start justify-start gap-y-4">
        <h1 className="text-2xl font-semibold">What is ShadCN?</h1>
        <p className="font-mono">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, hic placeat? Libero eos aliquid
          tenetur eius iure reprehenderit facilis provident velit molestias fuga atque eum voluptas, asperiores minima
          consectetur ab vel possimus ullam accusamus vero molestiae, delectus voluptates harum. Autem sapiente dolore
          dolores dolor repudiandae.
        </p>
        <div className="mt-4 flex items-center justify-start gap-x-5">
          <Button className="bg-cyan-500 text-white hover:bg-cyan-600" asChild>
            <Link href="/about">About ShadCN</Link>
          </Button>
          <Button className="bg-cyan-500 text-white hover:bg-cyan-600" asChild>
            <Link href="https://ui.shadcn.com/docs" target="_blank">
              Learn More
            </Link>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
