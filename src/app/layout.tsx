import type { Metadata } from "next";
import { PropsWithChildren } from "react";
import "./styles/index.css";
import Nav from "@/components/nav";
import ThemeProvider from "@/providers/ThemeProvider";

export const metadata: Metadata = {
  title: "ShadCN",
  description: "This is all about ShadCN",
  icons: {
    icon: "/favicon.ico",
  },
};

export default function RootLayout({ children }: Readonly<PropsWithChildren>) {
  return (
    <html lang="en" suppressHydrationWarning>
      <body>
        <ThemeProvider attribute="class" defaultTheme="system" enableSystem disableTransitionOnChange>
          <main className="mx-auto flex max-w-lg flex-col items-center py-24">
            <Nav />
            {children}
          </main>
        </ThemeProvider>
      </body>
    </html>
  );
}
