"use client";
import React from "react";
import DarkModeToggleButton from "./dark-mode-toggle";
import Link from "next/link";

const Nav: React.FC = () => {
  return (
    <div className="flex w-full items-center justify-between">
      <Link href="/" className="cursor-pointer text-xl font-bold">
        ShadCN
      </Link>
      <DarkModeToggleButton />
    </div>
  );
};

export default Nav;
